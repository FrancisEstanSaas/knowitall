//
//  Question.swift
//  KnowItAll
//
//  Created by Republisys on 20/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class Question {
    let questionText : String
    let answer : Bool
    
    init(text: String, correctAnswer: Bool) {
        questionText = text
        answer = correctAnswer
    }
}
