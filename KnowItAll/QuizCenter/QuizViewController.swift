//
//  QuizViewController.swift
//  KnowItAll
//
//  Created by Republisys on 27/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit


class QuizViewController: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerTiles: TargetAnswerView!
    @IBOutlet weak var answerLabel: UILabel!
    
    private let controller: GameController
    
    required init?(coder aDecoder: NSCoder) {
        controller = GameController()
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let level1 = Level(questionSet: "ImeeMarcos")
        print("anagrams: \(level1.questions)")
        
        let gameView = UIView(frame: CGRect(x:0, y:0, width:ScreenWidth, height:ScreenHeight))
        self.view.addSubview(gameView)
        controller.gameView = gameView
        
        //add one view for all hud and controls
        let hudView = HUDView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight))
        self.view.addSubview(hudView)
        controller.hud = hudView
        controller.hud.printSomething()
        
        //Exp
        //let answerView = TargetAnswerView()
        //self.answerTiles.addSubview(answerView)
        //self.view.addSubview(answerView)
        //controller.answerTile = answerView
        //answerView.setAnswerTiles(level: level1)
        //controller.answerTile.setAnswerTiles(level: level1)
        
        controller.level = level1
        controller.dealRandomAnagram()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
}
