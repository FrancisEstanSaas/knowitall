//
//  Config.swift
//  KnowItAll
//
//  Created by Republisys on 27/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

//UI Constants
let ScreenWidth = UIScreen.main.bounds.size.width
let ScreenHeight = UIScreen.main.bounds.size.height



//Random number generator
func randomNumber(min:UInt32, max:UInt32) -> Int {
    let result = (arc4random() % (max - min + 1)) + min
    return Int(result)
}
