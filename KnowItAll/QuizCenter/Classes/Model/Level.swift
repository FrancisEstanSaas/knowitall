//
//  Level.swift
//  KnowItAll
//
//  Created by Republisys on 27/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

struct Level {
    
    let pointsPerQuestion: Int
    let timeToSolve: Int
    let questions: [NSArray]
    
    init(questionSet: String) {
        // fin .plist file for this level
        let fileName = "\(questionSet).plist"
        let levelPath = "\(Bundle.main.resourcePath!)/\(fileName)"
        
        
        // load .plist file
        let levelDictionary: NSDictionary? = NSDictionary(contentsOfFile: levelPath)
        
        // validation
        assert(levelDictionary != nil, "Level configuration file not found")
        
        // initialize the object from the dictionary
        self.pointsPerQuestion = levelDictionary!["pointsPerQuestion"] as! Int
        self.timeToSolve = levelDictionary!["timeToSolve"] as! Int
        self.questions = levelDictionary!["questions"] as! [NSArray]
    }
    
}
