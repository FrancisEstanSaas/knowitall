//
//  GameController.swift
//  Anagrams
//
//  Created by Republisys on 26/04/2018.
//  Copyright © 2018 Caroline. All rights reserved.
//

import UIKit

class GameController: UIViewController {
    
    //stopwatch variables
    private var secondsLeft: Int = 0
    private var timer: Timer?
    
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerTiles: TargetAnswerView!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var hud: HUDView!
    

    var level: Level = Level(questionSet: "ImeeMarcos")
    let TileMargin: CGFloat = 2.0 //
    private var tiles = [TileView]()
    var targets = [UIImageView]() //private
    
    var distances = [UIImageView]()
    
//    init() {
//        print("GameController initialized")
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dealRandomAnagram()
    }
    
    func dealRandomAnagram() {
        
        //check that this method is only called after the level property is set & that level obj contains anagrams
        assert(level.questions.count > 0, "no level loaded")
        
        //generate a random index into the anagram list
        let randomIndex = randomNumber(min: 0, max: UInt32(level.questions.count-1))
        let anagramPair = level.questions[randomIndex]
        
        // store 2 phrases
        let anagram1 = anagramPair[0] as! String
        let anagram2 = anagramPair[1] as! String
        let anagram3 = anagramPair[2] as! String
        
        //
        print("\(anagram1)")
        print("\(anagram2)")
        print("\(anagram3)")
        
        //calculate the tile size
        let tileSide = ceil(ScreenWidth * 1.0 / CGFloat(max(anagram1.characters.count, anagram2.characters.count))) - TileMargin
        
        //get the left margin for first tile
        var xOffset = (ScreenWidth - CGFloat(max(anagram1.characters.count, anagram2.characters.count)) * (tileSide + TileMargin)) / 2.0
        
        //adjust for tile center (instead of the tile's origin)
        xOffset += tileSide / 2.0
        
        //code to display the tiles
//
//        //intialize the tile list
//        tiles = []
//
//        //create files
//        for (index, letter) in anagram1.enumerated() {
//
//            //3 check each letter
//            if letter != " " {
//                let tile = TileView(letter: letter, sideLength: tileSide)
//                tile.center = CGPoint(x: xOffset + CGFloat(index)*(tileSide + TileMargin), y: ScreenHeight/4*3)
//
//                //Part 2: tile
//                tile.dragDelegate = self
//
//                //4  add the tile to the gameView and tiles array
//                gameView.addSubview(tile)
//                tiles.append(tile)
//            }
//
//        }
//
    
            
        //for "TARGETS"
        targets = []
        
        
        //create targets
        for (index, letter) in anagram2.enumerated() {
            if letter != " " {
                let target = TargetView(letter: letter, sideLength: CGFloat(tileSide + answerTiles.TileMargin))
                target.center = CGPoint(x: xOffset + CGFloat(index)*(tileSide + TileMargin), y: answerTiles.TileMargin)
                
                
                targets.append(target)
                self.answerTiles.addSubview(target)
            }
        }
        
        
        

        
        //End of dealRandomAnagram
        
        self.questionLabel.text = anagram3
        self.answerLabel.text = anagram2
        
        self.stopStopwatch()
        
        
        
        
    }
    

    
    func placeTile(tileView: TileView, targetView: TargetView) {
        
        //1 check if phase completed = matching of tile and target
        targetView.isMatched = true
        tileView.isMatched = true
        
        //2 disble user interaction
        tileView.isUserInteractionEnabled = false
        
        //3
        UIView.animate(withDuration: 0.35,
                       delay: 0.00,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: {
                        tileView.center = targetView.center
                        tileView.transform = CGAffineTransform.identity
        }) { (value:Bool) in
            targetView.isHidden = true
        }
        
    }
    
//    func checkForSuccess() {
//        for targetView in targets {
//            //no success
//            if !targetView.isMatched {
//                return
//            }
//        }
//        print("Game Over - Try Again!")
//        //Later will do interesting things here XD
//        self.stopStopwatch()
//    }
    
    
    func startStopwatch() {
        secondsLeft = level.timeToSolve
        hud.stopwatch.setSeconds(seconds: secondsLeft)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: "tick:", userInfo: nil, repeats: true)
    }
    
    func stopStopwatch() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func tick(timer: Timer) {
        //to be checked
        secondsLeft - 1
        hud.stopwatch.setSeconds(seconds: secondsLeft)
        if secondsLeft == 0 {
            self.stopStopwatch()
        }
    }
    
}


extension GameController: TileDelegationProtocol {
    func tileView(tileView: TileView, didDragToPoint point: CGPoint) {
        var targetView: TargetView?
        for tv in targets {
//            if tv.frame.contains(point) && !tv.isMatched {
//                targetView = tv
//                break
//            }
        }
        
        // check whether the letter has been dragged to the correct target to make the anagram
        
        //1 check if target was found
        if let targetView = targetView {
            
            if targetView.letter == tileView.letter {
                print("Success! You should place tile here!")
                self.placeTile(tileView: tileView, targetView: targetView)
                
                //more stuff on success here
                print("Check if the player has completed the phrase")
                //self.checkForSuccess()
                
            } else {
                
                //4
                print("failure. Let the player know this tile doesn't belong here")
                //more stuff to do on failure here :D
                
                //1 insert Randomize()
                
                //2
                UIView.animate(withDuration: 0.35,
                               delay: 0.0,
                               options: UIViewAnimationOptions.curveEaseOut,
                               animations: {
                                tileView.center = CGPoint(x: tileView.center.x + CGFloat(randomNumber(min: 0, max: 40)-20), y: tileView.center.y + CGFloat(randomNumber(min: 20, max: 30)))
                }, completion: nil)
                
            }
        }
        
        
        
    }
}


