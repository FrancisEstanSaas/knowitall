//
//  HUDView.swift
//  Anagrams
//
//  Created by Republisys on 26/04/2018.
//  Copyright © 2018 Caroline. All rights reserved.
//

import UIKit


class HUDView: UIView {
    
    var stopwatch: StopwatchView

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init (frame:")
    }
    
    override init(frame: CGRect) {
        self.stopwatch = StopwatchView(frame: CGRect(x: ScreenWidth/2-150, y: 0, width: 300, height: 100))
        self.stopwatch.setSeconds(seconds: 0)
        
        super.init(frame: frame)
        self.addSubview(self.stopwatch)
        
        self.isUserInteractionEnabled = false
    }
    
    func printSomething() {
        print("Something XD")
    }
    
    
}
