//
//  TileView.swift
//  Anagrams
//
//  Created by Republisys on 26/04/2018.
//  Copyright © 2018 Caroline. All rights reserved.
//

import UIKit

protocol TileDelegationProtocol {
    func tileView(tileView: TileView, didDragToPoint: CGPoint)
}

// 1

class TileView: UIImageView {
    
    //PART 2: new properties
    private var xOffset: CGFloat = 0.0
    private var yOffset: CGFloat = 0.0
    var dragDelegate: TileDelegationProtocol?
    
    // 2
    var letter: Character
    // 3
    var isMatched: Bool = false
    // 4
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(letter:, sideLength:)")
    }
    // 5
    init(letter: Character) {
        self.letter = letter
        
        //5.1
        let image = UIImage(named: "tile")
        
        //5.2
        super.init(image: image)
        
        //6
//        let scale = sideLength / (image?.size.width)!
//        self.frame = CGRect(x: 0, y: 0, width: (image?.size.width)! * scale, height: (image?.size.height)! * scale)
    
        //Adding Letters
        
        //add a letter on top
        let letterLabel = UILabel(frame: self.bounds)
        letterLabel.textAlignment = NSTextAlignment.center
        letterLabel.textColor = UIColor.white
        letterLabel.backgroundColor = UIColor.clear
        letterLabel.text = String(letter).uppercased()
        letterLabel.font = UIFont(name: "Verdana-Bold", size: 20)
        self.addSubview(letterLabel)
        
        
        // PART 2:
        
        //instructs iOS to send touch events to this object.
        self.isUserInteractionEnabled = true
        
    }
    
    
    // ***randomize skipped
    
    
    // PART 2: Touch handling methods
    
    //1
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        

        if let touch = touches.first {
            let point = touch.location(in: self.superview)
            xOffset = point.x - self.center.x
            yOffset = point.y - self.center.y
        }
        //EXP
    }
    
    
    //2
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let point = touch.location(in: self.superview)
            self.center = CGPoint(x: point.x - xOffset, y: point.y - yOffset)
        }
        //EXP
    }
    
    //3
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchesMoved(touches, with: event)
        dragDelegate?.tileView(tileView: self,  didDragToPoint: self.center)
        
        //EXP
    }
    
    
}




























