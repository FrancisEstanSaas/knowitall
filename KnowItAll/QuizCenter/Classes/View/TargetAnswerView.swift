//
//  TargetAnswerView.swift
//  KnowItAll
//
//  Created by Republisys on 30/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class TargetAnswerView: UIView {

    
    
    //variables
    var level: Level!
    let TileMargin: CGFloat = 2.0 //
    var targets = [TargetView]() //private

    
    //init
    
    //functions
    func setAnswerTiles(level: Level) {
        //check that this method is only called after the level property is set & that level obj contains anagrams
        assert(level.questions.count > 0, "no level loaded")
        
        //generate a random index into the anagram list
        let randomIndex = randomNumber(min: 0, max: UInt32(level.questions.count-1))
        let anagramPair = level.questions[randomIndex]
        
        // store 2 phrases
        let anagram1 = anagramPair[0] as! String
        let anagram2 = anagramPair[1] as! String
        
        //
        print("\(anagram1)")
        print("\(anagram2)")
        
        //calculate the tile size
        let tileSide = ceil(ScreenWidth * 1.0 / CGFloat(max(anagram1.characters.count, anagram2.characters.count))) - TileMargin
        
        //get the left margin for first tile
        var xOffset = (ScreenWidth - CGFloat(max(anagram1.characters.count, anagram2.characters.count)) * (tileSide + TileMargin)) / 2.0
        
        //adjust for tile center (instead of the tile's origin)
        xOffset += tileSide / 2.0
        
        //for "TARGETS"
        targets = []
        
        //create targets
        for (index, letter) in anagram2.enumerated() {
            if letter != " " {
                let target = TargetView(letter: letter, sideLength: tileSide)
                target.center = CGPoint(x: xOffset + CGFloat(index)*(tileSide + TileMargin), y: ScreenHeight/4)
                
                self.addSubview(target)
                targets.append(target)
            }
        }
    }
    
}
