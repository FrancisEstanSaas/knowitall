//
//  StopwatchView.swift
//  Anagrams
//
//  Created by Republisys on 26/04/2018.
//  Copyright © 2018 Caroline. All rights reserved.
//

import UIKit


class StopwatchView: UILabel {
    
    //error check
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(frame:")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.font = UIFont(name: "Verdana-Bold", size: 40.0)

        
    }
    
    //helper method that implements time formatting
    //to an int parameter (eg the seconds left)
    
    func setSeconds(seconds: Int) {
        self.text = String(format: " %02i : %02i", seconds/60, seconds % 60)
    }
    
    
    
}






