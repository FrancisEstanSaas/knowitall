//
//  QuestionsViewController.swift
//  KnowItAll
//
//  Created by Republisys on 20/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import GoogleMobileAds

class QuestionsViewController: UIViewController {
    
    let allQuestions = QuestionBank()
    var pickedAnswer: Bool = false
    var questionNumber : Int = 0
    var score : Int = 0
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextQuestion()
        self.navigationController?.navigationBar.isHidden = true
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
    }
    
    
    @IBAction func answerPressed(_ sender: AnyObject) {
        if sender.tag == 1 {
            pickedAnswer = true
        } else if sender.tag == 2 {
            pickedAnswer = false
        }
        self.checkAnswer()
        questionNumber = questionNumber + 1
        self.nextQuestion()
    }
    
    
    func updateUI() {
        scoreLabel.text = "Score: \(score)"
        progressLabel.text = "\(questionNumber + 1) / 10"
        progressBar.frame.size.width = (view.frame.size.width / 10) * CGFloat(questionNumber + 1)
    }
    
    
    func nextQuestion() {
        if questionNumber <= 9 {
            questionLabel.text = allQuestions.list[questionNumber].questionText
            self.updateUI()
        } else {
            print("End of Quiz")
            let alert = UIAlertController(title: "Awesome", message: "You've finished all the questions, do you want to start over?", preferredStyle: .alert)
            let restartAction = UIAlertAction(title: "Restart", style: .default)  { (UIAlertAction) in
                self.startOver()
            }
            alert.addAction(restartAction)
            present(alert,animated: true,completion: nil)
        }
    }
    
    
    func checkAnswer() {
        let correctAnswer = allQuestions.list[questionNumber].answer
        if correctAnswer == pickedAnswer {
            //got it!
            ProgressHUD.showSuccess("Hooray!")
            score += 1
        } else {
            //wrong
            ProgressHUD.showError("Wrong!")
        }
    }
    
    
    func startOver() {
        score = 0
        questionNumber = 0
        nextQuestion()
        self.performSegue(withIdentifier: "goToMenu3", sender: self)
    }
    
    
    
}
