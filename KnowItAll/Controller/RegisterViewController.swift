//
//  RegisterViewController.swift
//  KnowItAll
//
//  Created by Republisys on 20/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//


import UIKit
import Firebase
import SVProgressHUD

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    
    //Pre-linked IBOutlets
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        emailTextfield.delegate = self
        emailTextfield.returnKeyType = .next
        
        passwordTextfield.delegate = self
        emailTextfield.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func registerPressed(_ sender: AnyObject) {
        
        SVProgressHUD.show()
        
        //TODO: Set up a new user on our Firbase database
        Auth.auth().createUser(withEmail: emailTextfield.text!, password: passwordTextfield.text!) { (user, error) in
            if error != nil {
                print(error!)
                SVProgressHUD.dismiss()
                ProgressHUD.showError("Login Error")
                //self.performSegue(withIdentifier: "goToRoot2", sender: self)
            } else {
                //Success
                print("Registration Successful")
                SVProgressHUD.dismiss()
                ProgressHUD.showSuccess("Registration successful!")
                self.performSegue(withIdentifier: "go2Menu2", sender: self)
                // go2Menu1 or goToMenu1
            }
        }
    }
    
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        passwordTextfield.resignFirstResponder()
        return true
    }
    
    
    
    
}


