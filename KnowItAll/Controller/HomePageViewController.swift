//
//  HomePageViewController.swift
//  KnowItAll
//
//  Created by Republisys on 23/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class HomePageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {

    var imageArray = [UIImage(named: "1"), UIImage(named: "2"), UIImage(named: "3"), UIImage(named: "4"), UIImage(named: "5")]
    var nameArray = ["Daniel Padilla", "Imee Marcos", "Paolo Duterte", "Sarah Duterte", "Sarah Geronimo"]

    
    @IBOutlet weak var idolCollectionView: UICollectionView!
    @IBOutlet weak var idolTableView: UITableView!
    @IBOutlet var leadingC: NSLayoutConstraint!
    @IBOutlet var trailingC: NSLayoutConstraint!
    
    @IBOutlet var homeSubView: UIView!
    
    var homeMenuIsVisible = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        idolCollectionView.dataSource = self
        idolCollectionView.delegate = self
        idolTableView.dataSource = self
        idolTableView.delegate = self
    }
    
    @IBAction func homeMenuBtnTapped(_ sender: Any) {
        //if the hamburger menu is NOT visible, then move the ubeView back to where it used to be
        if !homeMenuIsVisible {
            leadingC.constant = 150
            //this constant is NEGATIVE because we are moving it 150 points OUTWARD and that means -150
            trailingC.constant = -150
            
            //1
            homeMenuIsVisible = true
        } else {
            //if the hamburger menu IS visible, then move the ubeView back to its original position
            leadingC.constant = 0
            trailingC.constant = 0
            
            //2
            homeMenuIsVisible = false
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }

    @IBAction func signOutBtnTapped(_ sender: Any) {
        SVProgressHUD.show()
        do {
            try Auth.auth().signOut()
            //self.navigationController?.popToRootViewController(animated: true)
            SVProgressHUD.dismiss()
            ProgressHUD.showSuccess("Logout successful!")
            
            self.performSegue(withIdentifier: "logoutToHome", sender: self)
            // go2Menu1 or goToMenu1
        } catch let error {
            print(error)
        }
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Idol", for: indexPath) as! IdolsCollectionViewCell
        
        cell.idolImage.image = imageArray[indexPath.row]
        cell.idolLabel.text = nameArray[indexPath.row]

        
        
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "idolTable", for: indexPath) as! IdolTableViewCell
        cell.idolImage.image = imageArray[indexPath.row]
        cell.idolName.text = nameArray[indexPath.row]
        cell.idolQuizButton.tag = indexPath.row
        cell.idolQuizButton.addTarget(self, action: #selector(self.buttonTapped), for: UIControlEvents.touchUpInside)
        
        
        return cell
    }
    
    @objc func buttonTapped(sender: UIButton) {
        let buttonTag = nameArray[sender.tag]
            //sender.tag
        print(buttonTag)
    }
}
