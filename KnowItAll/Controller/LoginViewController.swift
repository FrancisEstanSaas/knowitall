//
//  LoginViewController.swift
//  KnowItAll
//
//  Created by Republisys on 20/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import GoogleMobileAds


class LoginViewController: UIViewController, GADInterstitialDelegate, UITextFieldDelegate {
    
    var interstitialAd : GADInterstitial!
    
    //Textfields pre-linked with IBOutlets
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func logInPressed(_ sender: AnyObject) {
        
        SVProgressHUD.show()
        //TODO: Log in the user
        Auth.auth().signIn(withEmail: emailTextfield.text!, password: passwordTextfield.text!) { (user, error) in
            if error != nil {
                print(error!)
                SVProgressHUD.dismiss()
                ProgressHUD.showError("Login Error")
                //self.performSegue(withIdentifier: "goToRoot1", sender: self)
            } else {
                print("Login successful!")
                SVProgressHUD.dismiss()
                ProgressHUD.showSuccess("Login successful!")
                
                self.performSegue(withIdentifier: "go2Menu1", sender: self)
                // go2Menu1 or goToMenu1
            }
        }
    }
    
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailTextfield.becomeFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        passwordTextfield.resignFirstResponder()
    }
    
} 



