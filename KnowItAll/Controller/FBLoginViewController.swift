//
//  FBLoginViewController.swift
//  KnowItAll
//
//  Created by Republisys on 20/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore


class FBLoginViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func loginWithFacebook(_ :AnyObject){
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
                ProgressHUD.showError("Login Error")
            case .cancelled:
                print("User cancelled login.")
                ProgressHUD.showError("Login Cancelled")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                let token = accessToken
                print("Logged in!", token.authenticationToken)
                self.getUserProfile()
            }
        }
    }
    
    
    @IBAction func cancelPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    func getUserProfile () {
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"], accessToken: AccessToken.current, httpMethod: GraphRequestHTTPMethod(rawValue: "GET")!, apiVersion: "2.8")) { httpResponse, result in
            print("result == ", result)
            switch result {
            case .success(let response):
                print("Graph Request Succeeded: \(response)")
                print("Custom Graph Request Succeeded: \(response)")
                print("My facebook id is \(response.dictionaryValue?["id"])")
                print("My name is \(response.dictionaryValue?["name"])")
                
                ProgressHUD.showSuccess("Login successful!")
                self.performSegue(withIdentifier: "go2Menu3", sender: self)
                // go2Menu3 or goToMenu3
                
            case .failed(let error):
                ProgressHUD.showError("Login Failed")
                print("Graph Request Failed: \(error)")
            }
        }
        
        connection.start()
    }

    
}
