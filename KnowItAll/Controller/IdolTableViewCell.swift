//
//  IdolTableViewCell.swift
//  KnowItAll
//
//  Created by Republisys on 23/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class IdolTableViewCell: UITableViewCell {

    
    @IBOutlet weak var idolImage: UIImageView!
    @IBOutlet weak var idolName: UILabel!
    @IBOutlet weak var idolQuizButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
