//
//  IdolsCollectionViewCell.swift
//  KnowItAll
//
//  Created by Republisys on 23/04/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class IdolsCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var idolImage: UIImageView!
    @IBOutlet weak var idolLabel: UILabel!
    
    
}
